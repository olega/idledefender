# IdleDefender

**TODO:**

- [x] Select the parameters in such a way that you can play at least 10 cycles of attacks on the village.
- [x] Add a field that displays the number of enemies in the next raid.
- [x] Add a parameter that shows how many turns from the beginning of the game enemies will start to appear. For example, in such a way that the first 3 cycles always have 0 methods in the raid, and then they start to come and their number increases.
- [x] The buttons are blocked if there is not enough grain, therefore, it is impossible to go into minus in terms of grain.
- [x] Complete the death screen: display the number of raids survived, grain produced, peasants and warriors, or, for example, fallen warriors - at your discretion.
- [x] Make a screen and a victory condition. Enter a variable under this condition: for example, hire 100 peasants or save 500 units of grain.
- [x] Make the button and pause screen.
- [x] Add SFX.
- [x] Add background music.
- [x] Make a mute button.
- [x] When harvesting, attacking funds, spawning peasants and warriors, eating grain, appropriate sounds should be played.
- [ ] Debug sound controller
- [ ] Change UI elements (buttons, panels, text fonts)
