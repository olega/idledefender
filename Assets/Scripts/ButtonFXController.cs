using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonFXController : MonoBehaviour
{
    public AudioSource myFx;
    public AudioClip hoverFx;
    public AudioClip clickFx;

    public void HoverFX()
    {
        myFx.PlayOneShot(hoverFx);
    }
    
    public void ClickFX()
    {
        myFx.PlayOneShot(clickFx);
    }
}
