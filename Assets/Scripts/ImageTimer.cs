using UnityEngine;
using UnityEngine.UI;

public class ImageTimer : MonoBehaviour
{
    public float maxTime;
    public bool tick;

    private Image _img;
    private float _currentTime;
    
    void Start()
    {
        _img = GetComponent<Image>();
        _currentTime = maxTime;
    }

    void Update()
    {
        tick = false;
        _currentTime -= Time.deltaTime;

        if (_currentTime <= 0)
        {
            tick = true;
            _currentTime = maxTime;
        }

        _img.fillAmount = _currentTime / maxTime;
    }
}
