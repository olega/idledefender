using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStarter : MonoBehaviour
{
    public GameObject prepareScreen;
    public GameObject gameScreen;
    
    public void OnButtonStartClicked()
    {
        prepareScreen.SetActive(false);
        gameScreen.SetActive(true);
        Time.timeScale = 1;
    }
}
