using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("Timers")]
    public ImageTimer harvestTimer;
    public ImageTimer eatingTimer;
    public Image raidTimerImg;
    public Image farmerTimerImg;
    public Image warriorTimerImg;
    
    [Header("Buttons")]
    public Button farmerButton;
    public Button warriorButton;

    [Header("Texts")]
    public Text farmerCountText;
    public Text warriorCountText;
    public Text foodCountText;
    public Text currentRaidInfo;
    public Text nextRaidInfo;

    [Header("Screens")]
    public GameObject loseScreenRaid;
    public GameObject loseScreensFood;
    public GameObject winScreen;

    [Header("Audio")] 
    public AudioSource musicHandler;
    public AudioSource soundFxHandler;
    public AudioClip harvestSound;
    public AudioClip raidSound;
    public AudioClip farmerSpawnSound;
    public AudioClip warriorSpawnSound;
    public AudioClip eatSound;
    public AudioClip loseSound;
    public AudioClip WinSound;

    [Header("Numerical values")]
    public int farmerCount;
    public int warriorCount;
    public int foodCount;

    public int foodPerFarmer;
    public int foodToWarrior;
    
    public int farmerCost;
    public int warriorCost;

    public float farmerCreateTime;
    public float warriorCreateTime;
    
    public float raidMaxTime;
    public int raidIncrease;
    public int nextRaid;
    public int raidWaveCounter = 1;
    public int raidStartFromWave;

    [Header("Statistic")]
    public Text statisticRaidCount;
    public Text statisticFoodCount;
    public Text statisticWinCount;
    public int farmerStatistic;
    public int warriorStatistic;
    public int foodStatistic;
    public int raidWaveStatistic;

    private float _farmerTimer = -2;
    private float _warriorTimer = -2;
    private float _raidTimer;
    
    void Start()
    {
        UpdateText();
        _raidTimer = raidMaxTime;
    }

    void Update()
    {
        _raidTimer -= Time.deltaTime;
        raidTimerImg.fillAmount = _raidTimer / raidMaxTime;
        
        if (_raidTimer <= 0)
        {
            _raidTimer = raidMaxTime;
            raidWaveCounter++;
            raidWaveStatistic++;
            
            if (raidWaveCounter >= raidStartFromWave)
            {
                soundFxHandler.PlayOneShot(raidSound);
                warriorCount -= nextRaid;
                nextRaid += raidIncrease;
            }
        }
        if (harvestTimer.tick)
        {
            foodCount += farmerCount * foodPerFarmer;
            foodStatistic += farmerCount * foodPerFarmer;
            soundFxHandler.PlayOneShot(harvestSound);
        }

        if (eatingTimer.tick)
        {
            foodCount -= warriorCount * foodToWarrior;
            soundFxHandler.PlayOneShot(eatSound);
        }

        if (_farmerTimer > 0)
        {
            _farmerTimer -= Time.deltaTime;
            farmerTimerImg.fillAmount = _farmerTimer / farmerCreateTime;
        }
        else if (_farmerTimer > -1)
        {
            farmerTimerImg.fillAmount = 1;
            farmerButton.interactable = true;
            farmerCount += 1;
            soundFxHandler.PlayOneShot(farmerSpawnSound);
            _farmerTimer = -2;
        }

        if (_warriorTimer > 0)
        {
            _warriorTimer -= Time.deltaTime;
            warriorTimerImg.fillAmount = _warriorTimer / warriorCreateTime;
        }
        else if (_warriorTimer > -1)
        {
            warriorTimerImg.fillAmount = 1;
            warriorButton.interactable = true;
            warriorCount += 1;
            soundFxHandler.PlayOneShot(warriorSpawnSound);
            _warriorTimer = -2;
        }

        if (foodCount < farmerCost)
        {
            farmerButton.interactable = false;
        }
        else if (_farmerTimer == -2.0f)
        {
            farmerButton.interactable = true;
        }
        
        if (foodCount < warriorCost)
        {
            warriorButton.interactable = false;
        }
        else if (_warriorTimer == -2.0f)
        {
            warriorButton.interactable = true;
        }
        
        UpdateText();

        WinLoseCondition();
    }

    public void CreateFarmer()
    {
        foodCount -= farmerCost;
        _farmerTimer = farmerCreateTime;
        farmerButton.interactable = false;
        farmerStatistic++;
    }
    
    public void CreateWarrior()
    {
        foodCount -= warriorCost;
        _warriorTimer = warriorCreateTime;
        warriorButton.interactable = false;
        warriorStatistic++;
    }
    
    private void UpdateText()
    {
        farmerCountText.text = farmerCount.ToString();
        warriorCountText.text = warriorCount.ToString();
        foodCountText.text = foodCount.ToString();
        currentRaidInfo.text = raidWaveCounter.ToString();
        nextRaidInfo.text = nextRaid.ToString();
    }

    private void WinLoseCondition()
    {
        if (warriorCount < 0)
        {
            Time.timeScale = 0;
            soundFxHandler.Pause();
            musicHandler.Pause();
            soundFxHandler.PlayOneShot(loseSound);
            loseScreenRaid.SetActive(true);
            statisticRaidCount.text = 
                farmerStatistic + 
                "\n" + warriorStatistic + 
                "\n" + foodStatistic + 
                "\n" + raidWaveStatistic;
        }

        if (foodCount < 0)
        {
            Time.timeScale = 0;
            foodCount = 0;
            soundFxHandler.Pause();
            musicHandler.Pause();
            soundFxHandler.PlayOneShot(loseSound);
            loseScreensFood.SetActive(true);
            statisticFoodCount.text = 
                farmerStatistic + 
                "\n" + warriorStatistic + 
                "\n" + foodStatistic + 
                "\n" + raidWaveStatistic;
        }
        
        if (raidWaveCounter >= 10 || foodCount >= 500)
        {
            Time.timeScale = 0;
            soundFxHandler.Pause();
            musicHandler.Pause();
            soundFxHandler.PlayOneShot(WinSound);
            winScreen.SetActive(true);
            statisticWinCount.text = 
                farmerStatistic + 
                "\n" + warriorStatistic + 
                "\n" + foodStatistic + 
                "\n" + raidWaveStatistic;
        }
    }
}
