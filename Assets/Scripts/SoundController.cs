using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    public AudioSource sound;
    public GameObject soundOff;
    
    private bool _muted;

    public void MuteGame()
    {
        if (_muted)
        {
            sound.Play();
            soundOff.SetActive(false);
        }
        else
        {
            sound.Pause();
            soundOff.SetActive(true);
        }
        
        _muted = !_muted;
    }
}
