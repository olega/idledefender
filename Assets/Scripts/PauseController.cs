using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseController : MonoBehaviour
{
    public AudioSource sound;
    public Sprite playSprite;
    public Sprite pauseSprite;
    public Image currentSprite;
    public Text onPaused;
    public GameObject pausePanel;
    
    private bool _paused;
    

    public void PausedGame()
    {
        if (_paused)
        {
            Time.timeScale = 1;
            sound.Play();
            currentSprite.sprite = pauseSprite;
            onPaused.text = "Пауза";
            pausePanel.SetActive(false);
        }
        else
        {
            Time.timeScale = 0;
            sound.Pause();
            currentSprite.sprite = playSprite;
            onPaused.text = "Играть";
            pausePanel.SetActive(true);
        }
        
        _paused = !_paused;
    }
}
